/****************************************************************************
 *   Copyright (c) 2017 Gabriel D. Bousquet. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name snap nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#pragma once

#include <array>
#include <atomic>
#include <cmath>
#include <cstdint>
#include <memory>
#include <mutex>

#include <Eigen/Dense>

#include <motoralloc/motoralloc.h>

#ifndef STRUCT_
#define STRUCT_
#include "structs.h"
#endif

namespace Snapdragon {

class ControllerManager
{
public:
  struct PIDParams {
    double Kp;
    double Kd;
    double Ki;
    double maxError = 0.;
    double maxInt = 0.;
  };

  struct InitParams {

    struct {
      double m;
      double Jx, Jy, Jz;
      Eigen::Vector3d r_bM; ///< CoM w.r.t bodyd origin, expressed in body
    } mechParams;

    struct {
      // pwmPercentage = a + sqrt(b + c*thrustN + d*thrustN**2)
      double a, b, c, d;
      double dragFactor; ///< propeller drag constant (c) [Nm/N]
      double Fmin, Fmax; ///< actuator limits [N]
    } propParams;

    struct {
      PIDParams roll;
      PIDParams pitch;
      PIDParams yaw;
    } controlParams;

    // motor geometry w.r.t body frame
    std::vector<acl::motoralloc::Motor> motors;
  };

  struct controlData {
    Quaternion q_des;
    Quaternion q_act;
    Quaternion q_err;
    Vector w_des;
    Vector w_act;
    Vector w_err;
    Vector s;
    Vector integrator;

    Eigen::Vector3d FBody; ///< desired forces expressed in the body frame
    Eigen::Vector3d MBody; ///< desired moment expressed in the body frame
    std::array<double, 8> fmix; ///< Traditionally mixed motor thrusts [N]
    std::array<double, 8> fopt; ///< Optimally allocated motor thrusts [N]
    Eigen::Vector3d slacks; ///< slacks from motoralloc---did we have to relax?
  };

public:
  ControllerManager();
  ~ControllerManager() = default;

  /**
  * Initalizes the Controller Manager with Controller Parameters
  * @param params
  *  The structure that holds the Controller parameters.
  * @return 
  *  0 = success
  * otherwise = failure.
  **/
  void Initialize(const InitParams& params);

  void updateCoM(const Eigen::Vector3d& rbm);

  void updateDesiredAttState(desiredAttState& desState, desiredAttState newdesState);
  void updateAttState(attState& attState, Quaternion q, Vector w);
  void updateMotorCommands(double dt, std::array<float, 6> &throttles, desiredAttState desState, attState attState );

  desiredAttState smc_des_;
  attState smc_state_;
  std::array<float, 6> throttles_;
  controlData smc_data_;
  Integrator RI, PI, YI;

private:
  std::atomic<bool> initialized_;
  std::atomic<bool> arm_;
  InitParams smc_params_;
  std::mutex sync_mutex_;
  std::unique_ptr<acl::motoralloc::MotorAlloc> motoralloc_;

  Eigen::Vector3d computeMomentCmd(double dt, const desiredAttState& desState, const attState& state);

  /**
   * @brief      Maps motor thrust in Newtons to normalized PWM value in [0, 1]
   *
   * @param[in]  f     Motor thrust in Newtons
   *
   * @return     Normalized PWM in [0, 1]
   */
  double fNewtonToPWM(double f);
};

} // ns Snapdragon
