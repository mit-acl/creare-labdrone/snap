/****************************************************************************
 *   Copyright (c) 2018 Gabriel D. Bousquet. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name snap nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include "SnapdragonControllerManager.hpp"
#include "SnapdragonUtils.hpp"

namespace Snapdragon {

Snapdragon::ControllerManager::ControllerManager() {
	initialized_ = false;
  throttles_.fill(0.0f);
}

// ----------------------------------------------------------------------------

void ControllerManager::Initialize(const InitParams& smc_params)
{
  smc_params_ = smc_params;
  smc_des_ = desiredAttState();

  smc_data_.FBody = Eigen::Vector3d::Zero();
  smc_data_.MBody = Eigen::Vector3d::Zero();
  smc_data_.fmix = { 0 };
  smc_data_.fopt = { 0 };
  smc_data_.slacks = Eigen::Vector3d::Zero();

  RI = Integrator(); PI = Integrator(); YI = Integrator();

  //
  // Setup optimal motor allocation
  //

  acl::motoralloc::Params params;
  params.fmin = smc_params_.propParams.Fmin;
  params.fmax = smc_params_.propParams.Fmax;
  params.c = smc_params_.propParams.dragFactor;
  params.r_bM = smc_params_.mechParams.r_bM;
  params.motors = smc_params_.motors;

  motoralloc_.reset(new acl::motoralloc::MotorAlloc(params));

  initialized_ = true;
}

// ----------------------------------------------------------------------------

void ControllerManager::updateCoM(const Eigen::Vector3d& rbm)
{
  motoralloc_->updateCoM(rbm);
}

// ----------------------------------------------------------------------------

void ControllerManager::updateDesiredAttState(desiredAttState& desState, desiredAttState newdesState)
{
  // Lock thread
  std::lock_guard<std::mutex> lock( sync_mutex_ );
  desState = newdesState;
}

// ----------------------------------------------------------------------------

void ControllerManager::updateAttState(attState& attState, Quaternion q, Vector w)
{
  // Lock thread
  std::lock_guard<std::mutex> lock( sync_mutex_ );
  attState.q = q;
  attState.w = w;
}

// ----------------------------------------------------------------------------

void ControllerManager::updateMotorCommands(double dt, std::array<float, 6>& throttles, desiredAttState desState, attState attState )
{
  if (desState.power && initialized_) {
    // Lock thread
    std::lock_guard<std::mutex> lock(sync_mutex_);

    const Eigen::Vector3d Fw(desState.F_W.x, desState.F_W.y, desState.F_W.z);
    const Eigen::Quaterniond q(attState.q.w, attState.q.x, attState.q.y, attState.q.z);

    smc_data_.FBody = q.inverse() * Fw;
    smc_data_.MBody = computeMomentCmd(dt, desState, attState);

    //
    // LP-based motor allocation
    //

    std::vector<double> fopt;
    motoralloc_->solve(smc_data_.FBody, smc_data_.MBody, fopt, smc_data_.slacks);
    std::copy_n(fopt.begin(), fopt.size(), smc_data_.fopt.begin());

    //
    // Traditional motor allocation
    //

    std::vector<double> fmix;
    motoralloc_->mix(smc_data_.FBody, smc_data_.MBody, fmix);
    std::copy_n(fmix.begin(), fmix.size(), smc_data_.fmix.begin());

    //
    // Map from Newtons to normalize PWM in [0, 1]
    //

    throttles[0] = (float) fNewtonToPWM(fopt[0]);
    throttles[1] = (float) fNewtonToPWM(fopt[1]);
    throttles[2] = (float) fNewtonToPWM(fopt[2]);
    throttles[3] = (float) fNewtonToPWM(fopt[3]);
    throttles[4] = (float) fNewtonToPWM(fopt[4]);
    throttles[5] = (float) fNewtonToPWM(fopt[5]);
  }
  else {
    throttles.fill(0.0f);

    RI.reset();
    PI.reset();
    YI.reset();
  }
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

Eigen::Vector3d ControllerManager::computeMomentCmd(double dt, const desiredAttState& desState,
                                                    const attState& attState)
{
  double sgn = 1;

  Quaternion qe;

  qConjProd(qe,attState.q,desState.q);
  static int count = 0;
  if (qe.w < 0) {
    sgn = -1;
    count++;
  }

  double pE = desState.w.x-attState.w.x;
  double qE = desState.w.y-attState.w.y;
  double rE = desState.w.z-attState.w.z;

  // Update smc_data struct
  smc_data_.q_des = desState.q;
  smc_data_.q_act = attState.q;
  smc_data_.q_err = qe;

  smc_data_.w_des = desState.w;
  smc_data_.w_act = attState.w;

  smc_data_.w_err.x = pE; smc_data_.w_err.y = qE; smc_data_.w_err.z = rE;
  smc_data_.integrator.x = RI.value; smc_data_.integrator.y = PI.value; smc_data_.integrator.z = YI.value;
    
  double rollCmd  = smc_params_.controlParams.roll.Kp*sgn*qe.x + smc_params_.controlParams.roll.Kd*pE;
  double pitchCmd = smc_params_.controlParams.pitch.Kp*sgn*qe.y + smc_params_.controlParams.pitch.Kd*qE;
  double yawCmd   = smc_params_.controlParams.yaw.Kp*sgn*qe.z + smc_params_.controlParams.yaw.Kd*rE;

  RI.increment(sgn*qe.x,dt);
  PI.increment(sgn*qe.y,dt);
  YI.increment(sgn*qe.z,dt);

  double maxIntRoll=smc_params_.controlParams.roll.maxInt;
  double maxIntPitch=smc_params_.controlParams.pitch.maxInt;
  double maxIntYaw=smc_params_.controlParams.yaw.maxInt;

  saturate(RI.value,-0.999*maxIntRoll,0.999*maxIntRoll);
  saturate(PI.value,-0.999*maxIntPitch,0.999*maxIntPitch);
  saturate(YI.value,-0.999*maxIntYaw,0.999*maxIntYaw);

  rollCmd  += RI.value * smc_params_.controlParams.roll.Ki;
  pitchCmd += PI.value * smc_params_.controlParams.pitch.Ki;
  yawCmd   += YI.value * smc_params_.controlParams.yaw.Ki;

  return Eigen::Vector3d{rollCmd, pitchCmd, yawCmd};
}

// ----------------------------------------------------------------------------

double ControllerManager::fNewtonToPWM(double f)
{
    const double a = smc_params_.propParams.a;
    const double b = smc_params_.propParams.b;
    const double c = smc_params_.propParams.c;
    const double d = smc_params_.propParams.d;

    // convert thrust (Newtons) to commanded PWM percentage
    double pwm = a + sqrt(b + c*f + d*f*f);

    pwm = saturate(pwm, 0.0, 1.0);
    return pwm;
}

} // ns Snapdragon
